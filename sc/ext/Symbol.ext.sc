+ Symbol {

	rateColor {
		^this.rateNumber.rateColor
	}

	rateNumber {
		^(trigger: 4,demand: 3,audio: 2,control: 1,scalar: 0).at(this)
	}

}
