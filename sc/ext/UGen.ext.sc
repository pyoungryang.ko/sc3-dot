+ UGen {

	dotColor {
		(this.inputs.size > Dot.truncateInputsAt).if {
			^"green"
		} {
			^this.rateColor.asString
		}
	}

	displayName {
		^this.name
	}

	dot { | aStream |
		^Dot.useTables.if {
			this.dotTable(aStream)
		} {
			this.dotRecord(aStream)
		}
	}

	dotEdge { | aStream dst i |
		// generate line of dot code to describe an edge in a ugen graph
		this.isControlProxy.if {
			aStream << "k_" << this.controlIndex.asString
		} {
			aStream << "u_" << this.synthIndex.asString << ":o_";
			this.isKindOf(OutputProxy).if {
			    aStream << this.outputIndex.asString
			} {
			    aStream << "0"
			};
			Dot.fixEdgeLocation.if {
			    aStream << ":s"
			}
		};
		aStream
			<< " -> u_" << dst.synthIndex.asString
			<< ":i_" << i.asString << " "
			<< "[color=" << this.dotColor << "];\n"
	}

	dotRecord { | aStream |
		// use dot record sub-language for node
		var esc = { | nm |
			["|","<",">"].indexOfEqual(nm).notNil.if {
			    "\\" ++ nm
			} {
			    nm
			}
		};
		aStream
			<< "u_" << this.synthIndex.asString << " "
			<< "[shape=record,color=" << this.dotColor
			<< ",label=\"{ { " << esc.value(this.displayName.asString);
		(this.inputs.size != 0).if {
			this.inputsTruncated.do { | i n |
			    var l = "";
			    Dot.drawInputName.if {
					l = this.argNameForInputAt(n) ++ ":"
				};
			    i.isNumber.if {
					l = l ++ i.asString
				};
			    aStream << "|<i_" << n.asString << "> " << l
			}
		};
		aStream << " }";
		(this.numOutputs != 0).if {
			aStream << "|{ ";
			this.numOutputs.do { | o |
			    aStream << "<o_" << o.asString << ">";
			    (o < (this.numOutputs - 1)).if {
					aStream << "|"
				}
			};
			aStream << " }"
		};
		aStream << " }\"];\n"
	}

	dotTable { | aStream |
		// use dot table sub-language (html) for node
		var esc = { | nm |
			(nm == "&").if {
				"&amp;"
			} {
				nm
			}
		};
		aStream
			<< "u_" << this.synthIndex.asString << " "
			<< "[shape=plaintext,color=" << this.dotColor
			<< ",label=<<TABLE BORDER=\"0\" CELLBORDER=\"1\">"
			<< "<TR><TD>" << esc.value(this.displayName.asString) << "</TD>";
		(this.inputs.size != 0).if {
			this.inputsTruncated.do { | i n |
			    var l = "";
			    Dot.drawInputName.if {
					l = this.argNameForInputAt(n) ++ ":"
				};
			    i.isNumber.if {
					l = l ++ i.asString
				};
			    aStream << "<TD PORT=\"" << "i_" << n.asString << "\">" << l << "</TD>"
			}
		};
		aStream << "</TR>";
		(this.numOutputs != 0).if {
			var d = this.numInputsTruncated + 1 - this.numOutputs;
			aStream << "<TR>";
			d.do {
				aStream << "<TD BORDER=\"0\"></TD>"
			};
			this.numOutputs.do { | o |
				aStream << "<TD PORT=\"o_" << o.asString << "\"></TD>"
			};
			aStream << "</TR>"
		};
		aStream << "</TABLE>>];\n"
	}

	inputsTruncated {
		(this.inputs.size > Dot.truncateInputsAt).if {
			('sc3-dot/Ugen>>inputsTruncated: ' ++ this.name).postln;
			^this.inputs.keep(Dot.truncateInputsAt)
		} {
			^this.inputs
		}
	}

	numInputsTruncated {
		^this.numInputs.min(Dot.truncateInputsAt)
	}

	rateColor {
		^this.rateNumber.rateColor
	}

}
