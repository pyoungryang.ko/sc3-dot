+ ControlName {

	dot { | aStream |
		aStream
			<< "k_" << this.index.asString << " "
			<< "[shape=rectangle,color=" << this.rate.rateColor.asString
			<< ",label=\"" << this.name.asString << ":" << this.defaultValue.asString << "\"];\n"
	}

}
