+ Integer {

	nameOfBinaryOpSpecialIndex {
		var nm = ["+", "-", "*", "IDiv", "/", "%", "==", "!=", "&lt;", "&gt;", "&lte;", "&gte;", "Min", "Max", "&", "|", "BitXor", "Lcm", "Gcd", "Round", "RoundUp", "Trunc", "Atan2", "Hypot", "Hypotx", "**", "ShiftLeft", "ShiftRight", "UnsignedShift", "Fill", "Ring1", "Ring2", "Ring3", "Ring4", "DifSqr", "SumSqr", "SqrSum", "SqrDif", "AbsDif", "Thresh", "AmClip", "ScaleNeg", "Clip2", "Excess", "Fold2", "Wrap2", "FirstArg", "RandRange", "ExpRandRange"];
		^nm.at(this).asSymbol
	}

	nameOfUnaryOpSpecialIndex {
		var nm = ["-", "!", "IsNil", "NotNil", "BitNot", "Abs", "AsFloat", "AsInt", "Ceil", "Floor", "Frac", "Sign", "Squared", "Cubed", "Sqrt", "Exp", "Recip", "MidiCps", "CpsMidi", "MidiRatio", "RatioMidi", "DbAmp", "AmpDb", "OctCps", "CpsOct", "Log", "Log2", "Log10", "Sin", "Cos", "Tan", "ArcSin", "ArcCos", "ArcTan", "SinH", "CosH", "TanH", "Rand_", "Rand2", "LinRand_", "BiLinRand", "Sum3Rand", "Distort", "SoftClip", "Coin", "DigitValue", "Silence", "Thru", "RectWindow", "HanWindow", "WelchWindow", "TriWindow", "Ramp_", "SCurve"];
		^nm.at(this).asSymbol
	}

}
